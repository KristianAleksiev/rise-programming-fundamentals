﻿namespace IntegerPow
{
    public class IntegerPow
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PLease enter a number:");
            int number = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter a pow:");
            int pow = int.Parse(Console.ReadLine());

            long result = Pow(number, pow);

            Console.WriteLine($"The result is: {result}");
        }

        public static long Pow(int a, int b)
        {
            return (long)Math.Pow(a, b);
        }
    }
}