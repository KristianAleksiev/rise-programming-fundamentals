﻿namespace DoubleFactorial
{
    public class DoubleFactorial
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number:");
            int number = int.Parse(Console.ReadLine());

            long calculation = GetDoubleFactorial(number);

            Console.WriteLine($"Result of {number} double factorial is: {calculation}");

        }


        public static long GetDoubleFactorial(int n)
        {
            if (n == 0 || n == 1)
                return 1;

            long result = 1;

            for (int current = n; current > 1; current--)
            {
                result *= current;
            }

            for (long current = result - 1; current > 1; current--)
            {
                result *= current;
            }
            return result;
        }

    }
}