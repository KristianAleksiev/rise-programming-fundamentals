﻿using System.Security.Cryptography.X509Certificates;

namespace OddEven
{
    public class OddEven
    {
        public static void Main()
        {
            Console.WriteLine("Please enter a number:");

            string input = Console.ReadLine();
            int number = int.Parse(input);

            bool oddOrEven = isOdd(number);

            if (oddOrEven)
                Console.WriteLine($"The number {number} is even.");
            else
                Console.WriteLine($"The number {number} is odd.");


        }
        static bool isOdd(int number){
            if (number % 2 == 0)
                return true;
            return false;
        }
    }
}