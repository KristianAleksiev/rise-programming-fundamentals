﻿namespace SumIntegersInString
{
    public class SumIntegersInString
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a string:");
            string inputStr = Console.ReadLine();

            int sumOfIntegersInStr = SumIntegersInAString(inputStr);
            Console.WriteLine($"The sum of the integers in the string is: {sumOfIntegersInStr}");
        }

        public static int SumIntegersInAString(string input)
        {
            int sum = 0;

            foreach (char current in input)
            {
                if (char.IsDigit(current))
                {
                    int currentEl = int.Parse(current.ToString());
                    sum += currentEl;
                }                    
            }
            return sum;
        }
    }
}