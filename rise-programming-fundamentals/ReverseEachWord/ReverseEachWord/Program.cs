﻿namespace ReverseEachWord
{
    public class ReverseEachWord
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a sentence:");
            string inputSentence = Console.ReadLine();

            ReverseStringWords(inputSentence);
        }

        public static void ReverseStringWords(string sentence)
        {

            string[] inputArray = sentence.Split(" ");
            Console.WriteLine(inputArray.Length);
            //string[] reversedArray = new string[inputArray.Length];

            for (int index = 0; index < inputArray.Length; index++)
            {
                char[] letterArr = inputArray[index].ToCharArray();
                Array.Reverse(letterArr);
                inputArray[index] = String.Concat(letterArr);
                Console.Write(inputArray[index] + " ");

            }

        }

    }
}