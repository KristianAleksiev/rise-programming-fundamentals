﻿using System.Collections.Immutable;

namespace KthMin
{
    public class KthMin
    {
        static void Main(String[] args)
        {
            Console.WriteLine("Enter the number of array elements:");

            string lengthString = Console.ReadLine();
            int arrayLength = int.Parse(lengthString);

            int[] elementsArray = new int[arrayLength];

            Console.WriteLine("Enter the elements:");
            for (int currentElement = 0; currentElement < arrayLength; currentElement++)
            {
                elementsArray[currentElement] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Enter K value:");
            string kElementsStr = Console.ReadLine();
            int kElements = int.Parse(kElementsStr);

            int minElemen = KthMinElement(kElements, elementsArray);
            Console.WriteLine($"The k-th min is: {minElemen}");

        }

        public static int KthMinElement(int k, int[] inputArray)
        {
            Array.Sort(inputArray);
            int kthMin = inputArray[k - 1];
            return kthMin;
        }


    }
}