﻿namespace MinElement
{
    public class MinElement
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number of array elements:");

            string lengthString = Console.ReadLine();
            int arrayLength = int.Parse(lengthString);

            int[] elementsArray = new int[arrayLength];

            Console.WriteLine("Enter the elements:");
            for (int currentElement = 0; currentElement < arrayLength; currentElement++)
            {
                elementsArray[currentElement] = int.Parse(Console.ReadLine());
            }

            int minElement = elementsArray.Min();
            Console.WriteLine($"Min element is: {minElement}");

        }
    }
}