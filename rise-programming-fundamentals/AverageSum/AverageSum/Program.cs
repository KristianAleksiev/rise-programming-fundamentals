﻿namespace AverageSum
{
    public class AverageSum
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number of array elements:");

            string lengthString = Console.ReadLine();
            int arrayLength = int.Parse(lengthString);

            int[] elementsArray = new int[arrayLength];

            Console.WriteLine("Enter the elements:");
            for (int currentElement = 0; currentElement < arrayLength; currentElement++)
            {
                elementsArray[currentElement] = int.Parse(Console.ReadLine());
            }
            int averageSum = GetAverage(elementsArray);


            Console.WriteLine($"The average sum of the elements in the array is: {averageSum}");
        }

        public static int GetAverage(int[] array)
        {
            int average = array.Sum() / array.Length;
            return average;
        }
    }
}