﻿namespace PrimeNumber
{
    public class PrimeNumber
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number:");

            string input = Console.ReadLine();
            int number = int.Parse(input);

            bool result = isPrime(number);

            if (result)
                Console.WriteLine($"Number {number} is a prime number.");
            else
                Console.WriteLine($"Number {number} is not a prime number");
        }

        public static bool isPrime(int number)
        {
            for (int current = 2; current < number; current++)
            {
                if (number % current == 0)
                    return false;
            }
            return true;
        }

    }
}