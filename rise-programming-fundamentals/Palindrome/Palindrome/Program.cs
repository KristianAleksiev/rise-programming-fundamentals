﻿namespace Palindrome
{
    public class Palindrome
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a string:");
            string inputString = Console.ReadLine();

            bool result = IsPalindrome(inputString);

            //Console.WriteLine(result);

            if (result)            
                Console.WriteLine("The string is a palindrome!");            
            else
                Console.WriteLine("The string is not a palindrome!");

        }
        public static bool IsPalindrome(string input)
        {
            char[] reversedStrArr = input.ToCharArray();
            Array.Reverse(reversedStrArr);

            string reversedStr = new string(reversedStrArr);

            if (input == reversedStr)
                return true;
            return false;
              
        }
    }
}