﻿namespace PalindromeInteger
{
    public class PalindromeInteger
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number:");
            int inputInt = int.Parse(Console.ReadLine());
            

            bool result = IsPalindrome(inputInt);

            //Console.WriteLine(result);

            if (result)
                Console.WriteLine("The number is a palindrome!");
            else
                Console.WriteLine("The number is not a palindrome!");

        }
        public static bool IsPalindrome(int input)
        {
            string strInput = input.ToString();

            char[] reversedStrArr = strInput.ToCharArray();
            Array.Reverse(reversedStrArr);

            string reversedStr = new string(reversedStrArr);

            if (strInput == reversedStr)
                return true;
            return false;

        }
    }
}